##################################################################
# Generic startup template for RFLPS Fast Interlock Module IOC
##################################################################

##################################################################
# Required modules and versions
##################################################################
require asyn,4.36.0
require nds3epics,1.0.1
require scaling,1.6.2
require autosave,5.10.0
require iocStats,3.1.16
require recsync,1.3.0-9705e52

### Main module of the IOC
#require fimioc,1.0.0-rc1
require fimioc,develop

##################################################################
# Environmental variables settings
##################################################################

### EPICS configurations
epicsEnvSet(EPICS_CA_MAX_ARRAY_BYTES, 10000000)
epicsEnvSet(SMNM, 8000)
epicsEnvSet(NELM, 8000)

### External file with individual macros for all input channels
iocshLoad("devicenames.iocsh")

### SEC-SUB prefix
epicsEnvSet(PREFIX, "LAB-010RFC")

### Calibration Files Folder
epicsEnvSet("CALIB_FOLDER", "$(PWD)/calibration")

##################################################################
# IFC1410 EPICS driver for RFLPS FIM firmware
##################################################################
ndsCreateDevice(ifcfastint, $(PREFIX), card=0, fmc=1, files=$(CALIB_FOLDER))
dbLoadRecords(FIMIOC_Main.template, "PREFIX=$(PREFIX), DEVICE=$(IOCDEVICE), NDSROOT=$(PREFIX), NDS0=FSM, SMNM=$(SMNM)")

##################################################################
# Analog Inputs Records
##################################################################
iocshLoad("dbLoad_aiChannelsRecs.iocsh")

##################################################################
# Digital Inputs Records
##################################################################
iocshLoad("dbLoad_diChannelsRecs.iocsh")

##################################################################
# Reflected Power Special Blocks Records
##################################################################
iocshLoad("dbLoad_refpwrChannelsRecs.iocsh")

##################################################################
# Process triggers for autosave restored PVs
##################################################################
iocshLoad("$(fimioc_DIR)/restore_pvs.iocsh")
afterInit("dbpf $(PREFIX):RestoreAll 1")

##################################################################
# Calc records for real-time preconditions evaluation
##################################################################
iocshLoad("$(fimioc_DIR)/preconditions.iocsh")

##################################################################
# autosave Module setup
##################################################################
epicsEnvSet("AS_REMOTE", "$(E3_CMD_TOP)/ifc1410-rflpslab1")
epicsEnvSet("AS_FOLDER", "savfiles")
iocshLoad("$(autosave_DIR)/autosave.iocsh", "AS_TOP=$(AS_REMOTE),IOCNAME=$(AS_FOLDER)")

##################################################################
# iocStats Module setup
##################################################################
epicsEnvSet("IOCSTATSPREFIX", "LAB-010")
iocshLoad("$(iocStats_DIR)/iocStats.iocsh", "IOCNAME=$(IOCSTATSPREFIX)")

##################################################################
# recSync Module setup
##################################################################
epicsEnvSet("RECSYNCPREFIX", "LAB-010")
iocshLoad("$(recsync_DIR)/recsync.iocsh", "IOCNAME=$(RECSYNCPREFIX)")


##################################################################
# Sequencer setup
##################################################################
afterInit('seq fimioc_automation_seq "PREFIX=$(PREFIX), DEVICE=$(IOCDEVICE), SIMDEV=RFS-SIM-110"')


##################################################################
# Launch IOC engine
##################################################################
iocInit()
